import entity.Column;
import entity.Filter;
import entity.Table;
import enums.ColumnType;
import enums.ConstraintType;
import service.InMemoryDatabaseService;
import service.InMemoryDatabaseServiceImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ServiceTest {

    public static void main(String[] args) {

        InMemoryDatabaseService service = new InMemoryDatabaseServiceImpl();

        Column columnId = new Column("id", ColumnType.INT);

        columnId.getConstraints().put(ConstraintType.INT_MAX, "2024");

        Column columnName = new Column("name", ColumnType.STRING);

        columnName.getConstraints().put(ConstraintType.NULL, "false");

        Table table = new Table("employee");
        table.setColumnList(Arrays.asList(columnId, columnName));

        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("id", "1500");
        columnMap.put("name", "kundan");


        try {
            //create table
            service.create(table);

            service.insert("employee", columnMap);
            columnMap.put("id", "1");
            columnMap.put("name", "ram");
            service.insert("employee", columnMap);
            columnMap.put("id", "10");
            columnMap.put("name", "hello");
            service.insert("employee", columnMap);
            Map<String, String> columnMap1 = new HashMap<>();
            columnMap1.put("id", "100");
            columnMap1.put("name", null);
            service.insert("employee", columnMap1);
            Filter filter = new Filter();
            filter.setFilterName("name");
            filter.setFilterValue("kundan");
            System.out.println(service.select("employee", null));
            System.out.println("-------------------");
            System.out.println(service.select("employee", Arrays.asList(filter)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
