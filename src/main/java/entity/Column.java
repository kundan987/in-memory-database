package entity;

import enums.ColumnType;
import enums.ConstraintType;

import java.util.HashMap;
import java.util.Map;

public class Column {

    String name;

    ColumnType type;

    Map<ConstraintType, String> constraints;

    public Column(String name, ColumnType type) {
        this.name = name;
        this.type = type;

        this.constraints = new HashMap<>();
        constraints.put(ConstraintType.NULL, "true");
        constraints.put(ConstraintType.STRING_MIN, "0");
        constraints.put(ConstraintType.STRING_MAX, "20");
        constraints.put(ConstraintType.INT_MIN, "-1024");
        constraints.put(ConstraintType.INT_MAX, "1024");

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ColumnType getType() {
        return type;
    }

    public void setType(ColumnType type) {
        this.type = type;
    }

    public Map<ConstraintType, String> getConstraints() {
        return constraints;
    }

    public void setConstraints(Map<ConstraintType, String> constraints) {
        this.constraints = constraints;
    }
}
