package entity;

import java.util.ArrayList;
import java.util.List;

public class Table {

    String name;

    List<Column> columnList;

    public Table(String name) {
        this.name = name;
        this.columnList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }
}
