package entity;

public class Record {

    String columnName;

    String columnValue;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnValue() {
        return columnValue;
    }

    public void setColumnValue(String columnValue) {
        this.columnValue = columnValue;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Record{");
        sb.append("columnName='").append(columnName).append('\'');
        sb.append(", columnValue='").append(columnValue).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
