package enums;

public enum ConstraintType {

    NULL, STRING_MIN, STRING_MAX, INT_MIN, INT_MAX;
}
