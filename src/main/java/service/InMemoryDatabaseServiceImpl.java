package service;

import entity.Column;
import entity.Filter;
import entity.Record;
import entity.Table;
import enums.ColumnType;
import enums.ConstraintType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryDatabaseServiceImpl implements InMemoryDatabaseService {

    Map<String, List<Column>> tableStructure = new HashMap<>();

    Map<String, List<List<Record>>> tableRecordMap = new HashMap<>();

    public void create(Table table) throws Exception{

        if (!tableExist(table.getName())){
            // validation on type and constraint limit
            tableStructure.put(table.getName(), table.getColumnList());
            tableRecordMap.put(table.getName(), new ArrayList<>());

        } else
            throw new Exception("Table already exist");

    }

    public void insert(String tableName, Map<String, String> columnMap) throws Exception{

        if (tableExist(tableName)){

            List<Column> columnList = tableStructure.get(tableName);
            List<Record> row = new ArrayList<>();
            for(Column column : columnList){

                if(columnMap.containsKey(column.getName())){

                    String colValue = columnMap.get(column.getName());

                    if(ColumnType.STRING.equals(column.getType())){

                        if (colValue != null && colValue.length() >= Integer.parseInt(column.getConstraints().get(ConstraintType.STRING_MIN)) &&
                                colValue.length() <= Integer.parseInt(column.getConstraints().get(ConstraintType.STRING_MAX))){
                            Record record = new Record();
                            record.setColumnName(column.getName());
                            record.setColumnValue(colValue);
                            row.add(record);
                        } else
                            throw new Exception("column length is not in defined limit");

                    } else {

                        if (Integer.parseInt(colValue) >= Integer.parseInt(column.getConstraints().get(ConstraintType.INT_MIN)) &&
                                Integer.parseInt(colValue) <= Integer.parseInt(column.getConstraints().get(ConstraintType.INT_MAX))){
                            Record record = new Record();
                            record.setColumnName(column.getName());
                            record.setColumnValue(colValue);
                            row.add(record);
                        } else
                            throw new Exception("column length is not in defined limit");
                    }
                } else{
                    if ("false".equals(column.getConstraints().get(ConstraintType.NULL))){
                        throw new Exception("mandatory column is missing");
                    }
                }
            }
            tableRecordMap.get(tableName).add(row);
        } else
            throw new Exception("Table not exist");

    }


    @Override
    public void deleteTable(String tableName) throws Exception {
        if (!tableExist(tableName)){
            tableRecordMap.remove(tableName);
            tableStructure.remove(tableName);

        } else
            throw new Exception("Table not exist");
    }

    @Override
    public List<List<Record>> select(String tableName, List<Filter> filters) throws Exception {

        if(tableExist(tableName)){

            if (filters == null || filters.size()==0)
                return tableRecordMap.get(tableName);
            else {
                //Assuming only one filter
                List<List<Record>> result = new ArrayList<>();
                Filter filter = filters.get(0);
                List<List<Record>> recordList = tableRecordMap.get(tableName);
                for(List<Record> records : recordList){
                    boolean flag = false;
                    for (Record record : records){
                        if (record.getColumnName().equals(filter.getFilterName()) && record.getColumnValue().equals(filter.getFilterValue()))
                            flag = true;
                    }
                    if (flag){
                        result.add(records);
                    }
                }
                return result;
            }
        }

        return new ArrayList<>();

    }

    private boolean tableExist(String tableName){
        return tableStructure.containsKey(tableName);
    }
}
