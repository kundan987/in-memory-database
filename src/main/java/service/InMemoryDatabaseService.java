package service;

import entity.Filter;
import entity.Record;
import entity.Table;

import java.util.List;
import java.util.Map;

public interface InMemoryDatabaseService {

    void create(Table table) throws Exception;

    void insert(String tableName, Map<String, String> columnMap) throws Exception;

    void deleteTable(String tableName) throws Exception;

    List<List<Record>> select(String tableName, List<Filter> filters) throws Exception;
}
